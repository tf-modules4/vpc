variable "region" {
  type        = string
  description = "Region"
  default     = "ap-south-1"
}

variable "cidr_block" {
  description = "The CIDR block for the VPC."
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_name" {
  description = "Name of the VPC to be created"
  type        = string
}

variable "enable_dns_support" {
  type    = bool
  default = true
}

variable "enable_dns_hostnames" {
  type    = bool
  default = true
}

variable "instance_tenancy" {
  type    = string
  default = "default"
}

variable "log_destination_type" {
  type    = string
  default = "s3"
}

variable "traffic_type" {
  type    = string
  default = "ALL"
}

variable "enable_vpc_logs" {
  type    = bool
  default = false
}

variable "logs_bucket" {
  description = "Name of bucket where we would be storing our logs"
}

variable "logs_bucket_arn" {
  description = "ARN of bucket where we would be storing vpc our logs"
}

variable "tags" {
  description = "Additional tags for the VPC"
  type        = map(string)
  default = {
    "owner"       = "devops"
    "env"         = "preprod"
    "provisioner" = "terraform"
  }
}